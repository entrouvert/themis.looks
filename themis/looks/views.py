from Products.Five import BrowserView
from Products.Five.browser.pagetemplatefile import ViewPageTemplateFile

from Products.CMFCore.utils import getToolByName

class BigBlocksFolderView(BrowserView):
    template = ViewPageTemplateFile('bigblocks_folder_view.pt')

    def __call__(self):
        return self.template()


class AllDocsFolderView(BrowserView):
    template = ViewPageTemplateFile('alldocs_folder_view.pt')

    def entries(self):
        catalog = getToolByName(self.context, 'portal_catalog')
        folder_path = '/'.join(self.context.getPhysicalPath())
        results = catalog.searchResults(path={'query': folder_path, 'depth': 1},
                sort_on='Date', sort_order='descending')
        return results

    def __call__(self):
        return self.template()

