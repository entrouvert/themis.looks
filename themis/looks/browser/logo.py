from plone.app.layout.viewlets.common import LogoViewlet
from Products.Five.browser.pagetemplatefile import ViewPageTemplateFile

class ThemisLogoViewlet(LogoViewlet):
    render = ViewPageTemplateFile('templates/logo.pt')
