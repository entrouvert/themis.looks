from zope.interface import Interface
from plone.theme.interfaces import IDefaultPloneLayer

class IThemeSpecific(IDefaultPloneLayer):
    """Marker interface that defines a Zope browser layer."""

class IBigBlocksFolderView(Interface):
    """Marker interface identifying Big Blocks Folder View."""

class IAllDocsFolderView(Interface):
    """Marker interface identifying All Docs Folder View"""
